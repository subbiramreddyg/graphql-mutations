package com.example.GraphDemo.demo.Entity;

import javax.persistence.*;
@Entity
public class Address {
	@Id
	@SequenceGenerator(
			name="address_seq",
			sequenceName="address_seq",
			allocationSize=1
			)
	@GeneratedValue(
			strategy=GenerationType.SEQUENCE,
			generator="address_seq"
			)
	private int aid;
	private String street;
	private String city;
	private String state;
	private String country;
	private int zip;
	
	
	public Address(int aid) {
	
		this.aid = aid;
	}
	public Address() {
		// TODO Auto-generated constructor stub
	}
	public int getaId() {
		return aid;
	}
	
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public int getZip() {
		return zip;
	}
	public void setZip(int zip) {
		this.zip = zip;
	}
	public Address(String street, String city, String state, String country, int zip) {
		
		this.street = street;
		this.city = city;
		this.state = state;
		this.country = country;
		this.zip = zip;
	}
	
	
}
