package com.example.GraphDemo.demo.Entity;

import javax.persistence.*;



@Entity
public class User {
	@Id
	@SequenceGenerator(
			name="user_seq",
			sequenceName="user_seq",
			allocationSize=1
			)
	@GeneratedValue(
			strategy=GenerationType.SEQUENCE,
			generator="user_seq"
			)
	private int id;
	private String name;
	@OneToOne(cascade = CascadeType.MERGE)
	@JoinColumn(
			name="qualification_id",
			referencedColumnName="qid"
			)
	private Qualification qualification;
	@OneToOne(cascade = CascadeType.MERGE)
	@JoinColumn(
			name="address_id",
			referencedColumnName="aid"
			)
	private Address address;
	public int getId() {
		return id;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Qualification getQualification() {
		return qualification;
	}
	public void setQualification(Qualification qualification) {
		this.qualification = qualification;
	}
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	public User( String name, Qualification qualification, Address address) {
		
		
		this.name = name;
		this.qualification = qualification;
		this.address = address;
	}
	public User() {
		
	}
	

}
