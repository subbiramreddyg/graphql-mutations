package com.example.GraphDemo.demo.Repository;



import org.springframework.data.jpa.repository.JpaRepository;


import com.example.GraphDemo.demo.Entity.User;


public interface UserRepository extends JpaRepository<User, Integer>{

}
