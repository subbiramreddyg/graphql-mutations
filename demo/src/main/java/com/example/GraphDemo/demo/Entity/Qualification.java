package com.example.GraphDemo.demo.Entity;


import javax.persistence.*;

@Entity
public class Qualification {
	@Id
	@SequenceGenerator(
			name="qual_seq",
			sequenceName="qual_seq",
			allocationSize=1
			)
	@GeneratedValue(
			strategy=GenerationType.SEQUENCE,
			generator="qual_seq"
			)
private int qid;
private String qdetails;
public int getQid() {
	return qid;
}

public String getQdetails() {
	return qdetails;
}
public void setQdetails(String qdetails) {
	this.qdetails = qdetails;
}
public Qualification(int qid) {

	this.qid = qid;
}
public Qualification(String qdetails) {
	
	this.qdetails = qdetails;
}
public Qualification() {
	// TODO Auto-generated constructor stub
}



}
